//
//  ViewController.swift
//  ExpandableContactList
//
//  Created by Alvin Luu on 8/8/18.
//  Copyright © 2018 None. All rights reserved.
//
// https://www.youtube.com/watch?v=mrKcyAOPiPM

import UIKit
import Contacts

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        addSlideMenuButton()
        
        fetchContacts()
    }

    private func fetchContacts() {
        print("attemping to fetch contacts today....")
        
        let store = CNContactStore()
        
        store.requestAccess(for: .contacts) { (granted, err) in
            if let err = err {
                print("Failed to request access:", err)
                return
            }
            
            if granted {
                print("Access granted")
                
                let keys = [CNContactGivenNameKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                do {
                    try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointerIfYouWantToStopEnumerating) in
                        
                        print(contact.givenName)
                    })
                }catch let err {
                    print("Failed to enumerate contacts:", err)
                }
            } else {
                print("Access denied")
            }
            
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

