//
//  MenuVC.swift
//  ExpandableContactList
//
//  Created by Alvin Luu on 8/8/18.
//  Copyright © 2018 None. All rights reserved.
//

import UIKit

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index: Int32)
    func onSlideMenuButtonPressed(_ sender : UIButton)
}

class MenuVC: UIViewController {
    
    @IBOutlet weak var btnMenu : UIButton!
    @IBOutlet weak var tableView: UITableView!
    var delegate : SlideMenuDelegate?
    let identifierNames = ["Contacts","Help","About"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    @IBAction func closeTapped(_ sender: Any) {
        delegate?.onSlideMenuButtonPressed(sender as! UIButton)
    }
    
    @IBAction func homeTapped(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let dvc = sb.instantiateViewController(withIdentifier: "ViewController")
        self.navigationController?.pushViewController(dvc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MenuVC: UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return identifierNames.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let identifier = identifierNames[indexPath.row]
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let dvc = sb.instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(dvc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        cell?.textLabel?.text = identifierNames[indexPath.row]
        cell?.textLabel?.textColor = .white
        
        return cell!
    }
}
